
// ======================================================================
//
//  Copyright (C) 2016  Rational Cyber LLC
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//  See the GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// ======================================================================

require([
    'jquery',
    'splunkjs/mvc',
    'splunkjs/mvc/searchmanager',
    'splunkjs/mvc/tableview',
    'splunkjs/mvc/textinputview',
    'splunkjs/mvc/timerangeview',
    'splunkjs/ready!',
    'splunkjs/mvc/simplexml/ready!'
], function (
    $,
    mvc,
    SearchManager,
    TableView,
    TextInputView,
    TimeRangeView
) {

    var splunkApp = undefined;
    var splunkDashboard = undefined;
    var splunkIndex = undefined;

    var splunkSourcetype = 'note';


    var splunkOneshotSearch = function (query, queryParams, callback) {
        var service = mvc.createService();
        service.login(function () {
            var jobs = service.jobs();
            jobs.oneshotSearch(query, queryParams, callback);
        });
    };


    var attachNoteHandlers = function () {

        $('form#dashboardNotesEntryForm').submit(function (event) {
            event.preventDefault();

            var form = this;

            // Get entered note and create Splunk collect query.
            var note = $(form).find('textarea[name="note"]').val();
            var splunkQuery = (
                '| makeresults' +
                    ' | eval Dashboard="' + splunkDashboard + '"' +
                    ' | eval Note="' + encodeURI(note) + '"' +
                    ' | makenote' +
                    ' | fields Timestamp User App Dashboard SID Note HMAC' +
                    ' | collect' +
                    '   index="' + splunkIndex + '"' +
                    '   sourcetype="' + splunkSourcetype + '"'
            );
            var splunkQueryParams = { };

            // Execute Splunk query to collect note onto index.
            // Schedule refresh of recent notes table.
            splunkOneshotSearch(
                splunkQuery,
                splunkQueryParams,
                function (err) {
                    if (err) {
                        console.error(err);
                        alert('Error: Could not save note.');

                        return;
                    }

                    // Refresh delay in milliseconds.
                    var refreshDelay = 2000;

                    form.reset();
                    setTimeout(function () {
                        mvc.Components.
                            getInstance('dashboardNotesSearch').
                            startSearch();
                    }, refreshDelay);
                });
        });

    };


    var attachNotesDisplay = function () {
        var dashboardNotesSearch = new SearchManager({
            id: 'dashboardNotesSearch',
            search: (
                'index="' + splunkIndex + '"' +
                    ' sourcetype="' + splunkSourcetype + '"' +
                    ' Timestamp=*' +
                    ' User="*$dashboardNotesSearchInputUser$*"' +
                    ' App="' + splunkApp + '"' +
                    ' (Dashboard="' + splunkDashboard + '"' +
                    '  OR (NOT Dashboard=*))' +
                    ' | eval Note=urldecode(Note)' +
                    ' | search Note="*$dashboardNotesSearchInputNote$*"' +
                    ' | table Timestamp User Note' +
                    ' | sort -Timestamp' +
                    ' | convert timeformat="%FT%T.%6N%Z" mktime(Timestamp)' +
                    ' | convert timeformat="%F %T" ctime(Timestamp)'
            ),
            earliest_time: '-30d@d',
            latest_time: 'now',
            preview: true,
            cache: true
        }, {tokens: true});

        var dashboardNotesTimeRange = new TimeRangeView({
            id: 'dashboardNotesTimeRange',
            managerid: 'dashboardNotesSearch',
            preset: 'Last 30 days',
            dialogOptions: {
                showPresets: true,
                showPresetsAllTime: true,
                showPresetsRealTime: false,
                showPresetsRelative: true,
                showCustom: true,
                showCustomAdvanced: true,
                showCustomDate: true,
                showCustomDateTime: false,
                showCustomRealTime: false,
                showCustomRelative: true,
                enableCustomAdvancedRealTime: false
            },
            el: $('#dashboardNotesTimeRangeContainer')
        }).render();

        var dashboardNotesSearchInputUser = new TextInputView({
            id: 'dashboardNotesSearchInputUser',
            value: mvc.tokenSafe('$dashboardNotesSearchInputUser$'),
            default: '*',
            el: $('#dashboardNotesSearchInputUserContainer')
        }).render();

        var dashboardNotesSearchInputNote = new TextInputView({
            id: 'dashboardNotesSearchInputNote',
            value: mvc.tokenSafe('$dashboardNotesSearchInputNote$'),
            default: '*',
            el: $('#dashboardNotesSearchInputNoteContainer')
        }).render();

        var dashboardNotesTable = new TableView({
            id: 'dashboardNotesTable',
            managerid: 'dashboardNotesSearch',
            drilldown: 'none',
            wrap: true,
            showPager: true,
            pagerPosition: 'bottom',
            pageSize: '5',
            el: $('#dashboardNotesTableContainer')
        }).render();

        dashboardNotesTimeRange.on('change', function () {
            dashboardNotesSearch.settings.set(dashboardNotesTimeRange.val());
        });

        dashboardNotesSearchInputUser.on('change', function () {
            mvc.Components.
                getInstance('dashboardNotesSearch').
                startSearch();
        });

        dashboardNotesSearchInputNote.on('change', function () {
            mvc.Components.
                getInstance('dashboardNotesSearch').
                startSearch();
        });

    };


    var loadNotesPanel = function () {
        $('#dashboardNotesPanel').load(
            '/static/app/dashboard_notes/notes-panel.html',
            function () {
                attachNoteHandlers();
                attachNotesDisplay();
            });
    };


    var settingsOk = function () {
        if (!splunkApp) {
            console.error('Could not determine Splunk app name.');

            return false;
        }
        if (!splunkDashboard) {
            console.error('Could not determine Splunk dashboard name.');

            return false;
        }
        if (!splunkIndex) {
            console.error('Could not determine Splunk index name.');

            return false;
        }
        if (!splunkSourcetype) {
            console.error('Splunk sourcetype was not set.');

            return false;
        }

        return true;
    };


    (function () {
        var pathName = window.location.pathname;
        var pathComponents = pathName.split('/');
        var i = pathComponents.indexOf('app');

        if (-1 === i) {
            console.error('Did not find "app" in URL pathname.');

            return;
        }

        splunkApp = pathComponents[i + 1];
        splunkDashboard = pathComponents[i + 2];

        var splunkQuery = (
            '| inputlookup dashboard_notes_index_map.csv' +
                ' | search' +
                '   app="' + splunkApp + '"' +
                '   dashboard="' + splunkDashboard + '"' +
                ' | fields index'
        );
        var splunkQueryParams = { };

        splunkOneshotSearch(
            splunkQuery,
            splunkQueryParams,
            function (err, results) {
                if (err) {
                    console.error(err);
                }

                if ('index' === results.fields[0]) {
                    splunkIndex = results.rows[0][0];
                }
                if (settingsOk()) {
                    loadNotesPanel();
                }
            }
        );
    }());

});
