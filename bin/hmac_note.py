#encoding: utf-8

# ======================================================================
#
#  Copyright (C) 2016  Rational Cyber LLC
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
#  See the GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# ======================================================================

import csv
import datetime
import hashlib
import hmac
import os
import re
import sys


def get_secret():
  # Obtain a secret for the HMAC
  secret = ""
  try:
    # Try to use the local splunk.secret; mileage may vary.
    # Customer's key management system could be used instead.
    splunk_secret_path = os.path.join(
      os.environ['SPLUNK_HOME'], 'etc', 'auth', 'splunk.secret'
    )
    with open(splunk_secret_path, 'rb') as f:
      secret = f.readline().strip()
  except:
    pass
  return secret


def get_rfc3339():
  return datetime.datetime.utcnow().isoformat() + 'Z'


def get_namespace(nstring):
  namespace = ''
  m = re.match('namespace\:(.*)', nstring)
  if m:
    namespace = m.group(1)
  return namespace


def get_sid(sidstring):
  sid = ''
  m = re.match('sid\:(.*)', sidstring)
  if m:
    sid = m.group(1)
  return sid


def get_username(authstring):
  username = ''
  m = re.match('authString\:\<auth\>\<userId\>.*\</userId\>\<username\>(.*)\</username\>', authstring)
  if m:
    username = m.group(1)
  return username


if  __name__ == '__main__':
  username = ''
  sid = ''
  namespace = ''
  row_content = 'start'

  secret = bytes(get_secret()).encode('utf-8')
  timestamp = bytes(get_rfc3339()).encode('utf-8')

  while (row_content != '\n') and (row_content != ''):
    row_content = sys.stdin.readline()
    if row_content.startswith('sid:'):
      sid = bytes(get_sid(row_content)).encode('utf-8')
    elif row_content.startswith('authString:'):
      username = bytes(get_username(row_content)).encode('utf-8')
    elif row_content.startswith('namespace:'):
      namespace = bytes(get_namespace(row_content)).encode('utf-8')

  csv_reader = csv.DictReader(sys.stdin)

  fieldnames = list(csv_reader.fieldnames)
  fieldnames.append('Timestamp')
  fieldnames.append('User')
  fieldnames.append('App')
  fieldnames.append('SID')
  fieldnames.append('HMAC')

  csv_writer = csv.DictWriter(sys.stdout, fieldnames)

  csv_writer.writeheader()
  for row in csv_reader:
    row['Timestamp'] = timestamp
    row['User'] = username
    row['App'] = namespace
    row['SID'] = sid
    if secret!="":
      row['HMAC'] = hmac.new(
        secret,
        row['Timestamp'] + row['User'] + row['App'] + row['Dashboard'] + row['SID'] + row['Note'],
        digestmod = hashlib.sha256
      ).hexdigest()
    else:
       row['HMAC'] = ""
    csv_writer.writerow(row)
