# Dashboard Notes

## The Problem We Were Trying To Solve

During a security compliance audit, a common question auditors ask
is for the system owner to prove that someone was actually monitoring
system logs on a regular basis. By default, Splunk logs user activity,
so, to satisfy this requirement, Splunk users can build dashboards that
show that they ran searches for the logs at certain times, or that they
loaded a given dashboard frequently enough. The problem with that solution,
though, is that it doesn't demonstrate that the user actually read the
search results and understood them, taking action if anything was wrong.
The problem we set out to solve was to make it easy to do just that.


## The Requirements We Defined

Build a widget that should be easy to drop into a typical Splunk
SimpleXML dashboard.

The widget should allow users to enter text notes describing what they're
seeing on the dashboard, as well as view past notes users have written
about that dashboard.

We assume that a dashboard is dedicated to a given system, i.e., that
any user who has access to that dashboard should see any notes written
about that dashboard, and that all notes about that specific dashboard
should live in one index.

The notes must be timestamped, include the username of the logged-in user,
and tamper-protected.


## What We Built

A custom Splunk command that takes an arbitrary note, the username of the
logged-in user, and the current system time, and generates an HMAC using
the search head's `splunk.secret` as its key.

A lookup table that maps dashboard names to indexes. When a user enters
a note on Dashboard `A`, our widget looks up the index associated with
Dashboard `A` in the lookup table, then stores the note and the HMAC in
that index.

A GUI widget that admins can add to any Splunk SimpleXML dashboard with
a few lines of code. The widget has a text area for entering notes and
a table of recent notes.


## Instructions for Including the Widget in a Dashboard

Edit `$SPLUNK_HOME/apps/dashboard_notes/lookups/dashboard_notes_index_map.csv`
to include an entry for the new dashboard and to tie it to the correct index.
Typically, that index would be the same index that the dashboard primarily
pulls its data from, as that will most likely have the correct permissions.
For government or otherwise highly regulated users, we highly recommend
separating indexes by FISMA system, as doing so greatly simplifies compliance
and enforcing the principle of least privilege.

Edit Source on the dashboard to which you are adding the widget.

Modify the first line of the SimpleXML (which should be a `<form>`
or a `<dashboard>`) to include the JavaScript and CSS the widget.
~~~ xml
<form script="dashboard_notes:notes-panel.js"
      stylesheet="dashboard_notes:notes-panel.css">
~~~

Add the following SimpleXML wherever you want the widget to appear.
We find it usually works well at the bottom of a dashboard.
~~~ xml
<row>
  <panel>
    <html id="dashboardNotesPanel"/>
  </panel>
</row>
~~~

You will need to `debug/refresh` and `_bump` the Splunk search head
or restart the Splunk search head for the included JavaScript and CSS
to take effect on the modified dashboards.

Show your users how they can use the widget to demonstrate to auditors
that they're actively monitoring system logs.


## Other Use Cases

While we built this app to solve a compliance problem, we've found it
handy for non-compliance dashboards as well. Sometimes you just need a
convenient place to take notes about a dashboard, such as when you're
investigating an incident or troubleshooting a problem.
We're interested in improving the app for those use cases as well.
